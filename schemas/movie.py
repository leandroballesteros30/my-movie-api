from pydantic import BaseModel,Field
from typing import Optional

class Movie(BaseModel):
    id: Optional [int] = None
    title: str = Field(default="Mi peli",min_length=5, max_length=15) # Por medio de la libreria Field podemos hacer restricciones o validaciones de datos
    overview: str = Field(default="abcdefe",min_length=5, max_length=100)
    year: int = Field(default=2023, le=2023)
    rating: float
    category: str